<?php

use \yii\bootstrap\Button;
use \yii\bootstrap\ButtonGroup;
use yii\bootstrap\Html;
use yii\data\ArrayDataProvider;


/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-3">
            left
        </div>
        <div class="col-sm-6">
            <div class="row" align="center">
                <div class="col-sm-2">
                    <?= //Кнопка выбора предыдущего месяца
                    Html::a(Yii::t('app', '<'), ['/',
                        'm' => $currMonth,
                        'dirrection' => 'left',
                        'y' => $currYear], ['class' => 'btn btn-primary', 'style' => 'padding-right:15px;']) ?>
                </div>
                <div class="col-sm-8">
                    <h1><?= $currMonth . ', ' . $currYear ?></h1>
                </div>
                <div class="col-sm-2">
                    <?= //Кнопка выбора следующего месяца
                    Html::a(Yii::t('app', '>'), ['/',
                        'm' => $currMonth,
                        'dirrection' => 'right',
                        'y' => $currYear], ['class' => 'btn btn-primary', 'style' => 'padding-right:15px;']) ?>
                </div>
            </div>

            <table class="table"  >
                <thead style="background: #fc0">
                <tr >
                    <th style="text-align:center">пн</th>
                    <th style="text-align:center">вт</th>
                    <th style="text-align:center">ср</th>
                    <th style="text-align:center">чт</th>
                    <th style="text-align:center">пт</th>
                    <th style="text-align:center">сб</th>
                    <th style="text-align:center">вс</th>
                </tr>
                </thead>
                <tbody>
                <?

                for ($i = 0; $i < 6; $i++) {
                    echo '<tr>
                    <td style="color:' . $colorArray[$i * 7 + 0] . '" align="center">' . $dateArray[$i * 7 + 0] . '</td>
                    <td style="color:' . $colorArray[$i * 7 + 1] . '" align="center">' . $dateArray[$i * 7 + 1] . '</td>
                    <td style="color:' . $colorArray[$i * 7 + 2] . '" align="center">' . $dateArray[$i * 7 + 2] . '</td>
                    <td style="color:' . $colorArray[$i * 7 + 3] . '" align="center">' . $dateArray[$i * 7 + 3] . '</td>
                    <td style="color:' . $colorArray[$i * 7 + 4] . '" align="center">' . $dateArray[$i * 7 + 4] . '</td>
                    <td style="color:' . $colorArray[$i * 7 + 5] . '" align="center">' . $dateArray[$i * 7 + 5] . '</td>
                    <td style="color:' . $colorArray[$i * 7 + 6] . '" align="center">' . $dateArray[$i * 7 + 6] . '</td>
                </tr>';
                }

                ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-3">
            right
        </div>
    </div>
</div>


