<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


class SiteController extends Controller
{
    public function init()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public $monthInRussianList = [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь',
    ];
    public $monthInEnglishList = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ];

    private function getMonthInRussian($month)
    {
        switch ($month) {
            case 'January':
                return 'Январь';
            case 'February':
                return 'Февраль';
            case 'March':
                return 'Март';
            case 'April':
                return 'Апрель';
            case 'May':
                return 'Май';
            case 'June':
                return 'Июнь';
            case 'July':
                return 'Июль';
            case 'August':
                return 'Август';
            case 'September':
                return 'Сентябрь';
            case 'October':
                return 'Октябрь';
            case 'November':
                return 'Ноябрь';
            case 'December':
                return 'Декабрь';
        }
    }

    private function getMonthInDigit($month)
    {
        switch ($month) {
            case 'January':
                return '01';
            case 'February':
                return '02';
            case 'March':
                return '03';
            case 'April':
                return '04';
            case 'May':
                return '05';
            case 'June':
                return '06';
            case 'July':
                return '07';
            case 'August':
                return '08';
            case 'September':
                return '09';
            case 'October':
                return '10';
            case 'November':
                return '11';
            case 'December':
                return '12';
        }
    }

    private function getMonthInEnglish($month)
    {
        switch ($month) {
            case 'Январь':
                return 'January';
            case 'Февраль':
                return 'February';
            case 'Март':
                return 'March';
            case 'Апрель':
                return 'April';
            case 'Май':
                return 'May';
            case 'Июнь':
                return 'June';
            case 'Июль':
                return 'July';
            case 'Август':
                return 'August';
            case 'Сентябрь':
                return 'September';
            case 'Октябрь':
                return 'October';
            case 'Ноябрь':
                return 'November';
            case 'Декабрь':
                return 'December';
        }
    }

    private function getPreviosMonth($month)
    {
        switch ($month) {
            case 'January':
                return 'December';
            case 'February':
                return 'January';
            case 'March':
                return 'February';
            case 'April':
                return 'March';
            case 'May':
                return 'April';
            case 'June':
                return 'May';
            case 'July':
                return 'June';
            case 'August':
                return 'July';
            case 'September':
                return 'August';
            case 'October':
                return 'September';
            case 'November':
                return 'October';
            case 'December':
                return 'November';
        }
    }

    private function getNextMonth($month)
    {
        switch ($month) {
            case 'January':
                return 'February';
            case 'February':
                return 'March';
            case 'March':
                return 'April';
            case 'April':
                return 'May';
            case 'May':
                return 'June';
            case 'June':
                return 'July';
            case 'July':
                return 'August';
            case 'August':
                return 'September';
            case 'September':
                return 'October';
            case 'October':
                return 'November';
            case 'November':
                return 'December';
            case 'December':
                return 'January';
        }
    }


    public $colors = [
        'disable' => '#a3a3c2',
        'active' => '#000000',
        'output' => '#ff0000',
        'holiday' => '#ffcc00',
        'today' => '#008000',
    ];

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($m = null, $y = null, $dirrection = null)
    {


        $dateArray = []; //Массив с датами для текущего месяца
        $colorArray = []; //Массив цветов для текущего месяца

        $currMonth = date('F');
        $currYear = date('o');
        $firstDay = date('d');
        $lastDayWeekOfPreviosMonth = new \DateTime(date('Y-m-d H:i:s'));
        $date = $currentDate = new \DateTime(date('Y-m-d H:i:s'));



        if (!empty($m) || !empty($y) || !empty($dirrection)) {
            if (in_array($m, $this->monthInRussianList)) {
                if ($dirrection == 'leftMonth'
                    || $dirrection == 'rightMonth'
                    || $dirrection == 'rightYear'
                    || $dirrection == 'leftYear'
                    || $dirrection == 'straight'
                ) {
                    if (is_numeric($y) && $y > 0 && $y < 9999) {
                        $currYear = $y;
                        $currMonth = $this->getMonthInEnglish($m);


                        switch ($dirrection) {
                            case 'leftMonth':
                                if ($currMonth == 'January') {
                                    $currYear--;
                                    $currMonth = 'December';
                                } else {
                                    $currMonth = $this->getPreviosMonth($currMonth);
                                }
                                break;
                            case 'rightMonth':
                                if ($currMonth == 'December') {
                                    $currYear++;
                                    $currMonth = 'January';
                                } else {
                                    $currMonth = $this->getNextMonth($currMonth);
                                }
                                break;
                            case 'rightYear':
                                $currYear++;
                                break;
                            case 'leftYear':
                                $currYear--;
                                break;
                            case 'straight':
                                break;
                        }

                        $date = \DateTime::createFromFormat('j-M-Y', '01-' . $currMonth . '-' . $currYear);

                        $lastDayWeekOfPreviosMonth = \DateTime::createFromFormat('j-M-Y', '01-' . $currMonth . '-' . $currYear);
                        $currentDate = $date;
                        $firstDay = $date->format('d');
                        //return $firstDay. '|' . $date->format('Y-m-d H:i:s') . '|' . $currMonth;
                    }
                }
            }
        }


        //Вычисляем цифру актуального месяца
        empty($m) ? $currMonthInDigit = date('m') : $currMonthInDigit = $this->getMonthInDigit($currMonth);


        $lastDayWeekOfPreviosMonth->sub(new \DateInterval('P' . $firstDay . 'D'));
        //return $lastDayWeekOfPreviosMonth->format('Y-m-d H:i:s').'|'.$date->format('Y-m-d H:i:s').'|'.$firstDay;
        //Если первый день месяца выпадает на понедельник
        $val = $lastDayWeekOfPreviosMonth->format('N'); //День недели предыдущего месяца
        $lastMonthNumDay = $lastDayWeekOfPreviosMonth->format('d'); //Номер дня предыдущего месяца
        $countDaysInCurrentMonth = $date->format('t');//Количетсво дней в текущем месяце

        //return $countDaysInCurrentMonth.'|'.$date->format('Y-m-d H:i:s');
        if ($val == 7) {
            //Добавляем неактивных семь дней предыдущего месяца
            $dateArray[0] = $lastMonthNumDay - 6;
            $dateArray[1] = $lastMonthNumDay - 5;
            $dateArray[2] = $lastMonthNumDay - 4;
            $dateArray[3] = $lastMonthNumDay - 3;
            $dateArray[4] = $lastMonthNumDay - 2;
            $dateArray[5] = $lastMonthNumDay - 1;
            $dateArray[6] = $lastMonthNumDay;
            $colorArray[0] = $colorArray[1] = $colorArray[2] = $colorArray[3] = $colorArray[4] = $colorArray[5] = $colorArray[6] = $this->colors['disable'];
            for ($i = 7; $i < $countDaysInCurrentMonth + 7; $i++) {
                $dateArray[$i] = $i - 6;
                $colorArray[$i] = $this->colors['active'];
            }
            for ($i = $countDaysInCurrentMonth + 7; $i < 42; $i++) {
                $dateArray[$i] = $i - $countDaysInCurrentMonth - 6;
                $colorArray[$i] = $this->colors['disable'];
            }

        } else { //Если первый день месяца выпадает на любой день кроме понедельника
            ////Добавляем дни до полной коллекции в первую строчку
            //Текущий день недели
            switch ($val) {
                case 1;
                    $dateArray[0] = $lastMonthNumDay - 0;
                    $colorArray[0] = $this->colors['disable'];
                    for ($i = 1; $i < $countDaysInCurrentMonth + 1; $i++) {
                        $dateArray[$i] = $i;
                        $colorArray[$i] = $this->colors['active'];
                    }
                    for ($i = $countDaysInCurrentMonth + 1; $i < 42; $i++) {
                        $dateArray[$i] = $i - $countDaysInCurrentMonth - 0;
                        $colorArray[$i] = $this->colors['disable'];
                    }
                    break;
                case 2;
                    $dateArray[0] = $lastMonthNumDay - 1;
                    $dateArray[1] = $lastMonthNumDay - 0;
                    $colorArray[0] = $colorArray[1] = $this->colors['disable'];
                    for ($i = 2; $i < $countDaysInCurrentMonth + 2; $i++) {
                        $dateArray[$i] = $i - 1;
                        $colorArray[$i] = $this->colors['active'];
                    }
                    for ($i = $countDaysInCurrentMonth + 2; $i < 42; $i++) {
                        $dateArray[$i] = $i - $countDaysInCurrentMonth - 1;
                        $colorArray[$i] = $this->colors['disable'];
                    }
                    break;
                case 3;
                    $dateArray[0] = $lastMonthNumDay - 2;
                    $dateArray[1] = $lastMonthNumDay - 1;
                    $dateArray[2] = $lastMonthNumDay - 0;
                    $colorArray[0] = $colorArray[1] = $colorArray[2] = $this->colors['disable'];
                    for ($i = 3; $i < $countDaysInCurrentMonth + 3; $i++) {
                        $dateArray[$i] = $i - 2;
                        $colorArray[$i] = $this->colors['active'];
                    }
                    for ($i = $countDaysInCurrentMonth + 3; $i < 42; $i++) {
                        $dateArray[$i] = $i - $countDaysInCurrentMonth - 2;
                        $colorArray[$i] = $this->colors['disable'];
                    }
                    break;
                case 4;
                    $dateArray[0] = $lastMonthNumDay - 3;
                    $dateArray[1] = $lastMonthNumDay - 2;
                    $dateArray[2] = $lastMonthNumDay - 1;
                    $dateArray[3] = $lastMonthNumDay - 0;
                    $colorArray[0] = $colorArray[1] = $colorArray[2] = $colorArray[3] = $this->colors['disable'];
                    for ($i = 4; $i < $countDaysInCurrentMonth + 4; $i++) {
                        $dateArray[$i] = $i - 3;
                        $colorArray[$i] = $this->colors['active'];
                    }
                    for ($i = $countDaysInCurrentMonth + 4; $i < 42; $i++) {
                        $dateArray[$i] = $i - $countDaysInCurrentMonth - 3;
                        $colorArray[$i] = $this->colors['disable'];
                    }
                    break;
                case 5;
                    $dateArray[0] = $lastMonthNumDay - 4;
                    $dateArray[1] = $lastMonthNumDay - 3;
                    $dateArray[2] = $lastMonthNumDay - 2;
                    $dateArray[3] = $lastMonthNumDay - 1;
                    $dateArray[4] = $lastMonthNumDay - 0;
                    $colorArray[0] = $colorArray[1] = $colorArray[2] = $colorArray[3] = $colorArray[4] = $this->colors['disable'];
                    for ($i = 5; $i < $countDaysInCurrentMonth + 5; $i++) {
                        $dateArray[$i] = $i - 4;
                        $colorArray[$i] = $this->colors['active'];
                    }
                    for ($i = $countDaysInCurrentMonth + 5; $i < 42; $i++) {
                        $dateArray[$i] = $i - $countDaysInCurrentMonth - 4;
                        $colorArray[$i] = $this->colors['disable'];
                    }
                    break;
                case 6;
                    $dateArray[0] = $lastMonthNumDay - 5;
                    $dateArray[1] = $lastMonthNumDay - 4;
                    $dateArray[2] = $lastMonthNumDay - 3;
                    $dateArray[3] = $lastMonthNumDay - 2;
                    $dateArray[4] = $lastMonthNumDay - 1;
                    $dateArray[5] = $lastMonthNumDay - 0;
                    $colorArray[0] = $colorArray[1] = $colorArray[2] = $colorArray[3] = $colorArray[4] = $colorArray[5] = $this->colors['disable'];
                    for ($i = 6; $i < $countDaysInCurrentMonth + 6; $i++) {
                        $dateArray[$i] = $i - 5;
                        $colorArray[$i] = $this->colors['active'];
                    }
                    for ($i = $countDaysInCurrentMonth + 6; $i < 42; $i++) {
                        $dateArray[$i] = $i - $countDaysInCurrentMonth - 5;
                        $colorArray[$i] = $this->colors['disable'];
                    }
                    break;
            }


        }

        //Находим текущий день и устанавливаем цвет для него
        //todo можно автоматизировать наверное этот кусок кода
        $currDay =date('d');
        $positionCurrDay = 0;
        $flagMonthBegin = false;
        for ($i=0; $i<42; $i++) {
            if ($dateArray[$i] == 1 )
            {
                $flagMonthBegin = true;
            }
            if ($flagMonthBegin && $dateArray[$i] == $currDay) {
                $positionCurrDay = $i;
                break;
            }
        }
        $colorArray[$positionCurrDay] = $this->colors['today'];


        return [
            'currYear' => $currYear,
            'currMonth' => $this->getMonthInRussian($currMonth),
            'currMonthEnglish' => $currMonth,
            'currMonthDigit' => $currMonthInDigit,
            'currDay' => $currDay,
            'dateArray' => $dateArray,
            'colorArray' => $colorArray,
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}