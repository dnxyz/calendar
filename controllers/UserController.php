<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Day;


class UsersController extends Controller
{
    //Контроллер возвращает всех пользаков
    public function actionGet()
    {
        $model = New User();
        $result = $model->getAllUsers();

         Yii::$app->response->headers->set('X-Total-Count', [count ($result)]);
        return $result;
    }

    //Контроллер возвращает одного пользака
    public function actionIndex($id)
    {
        $model = New User();
        $result = $model->getuserbyid($id);
е
        //Yii::$app->response->headers->set('X-Total-Count', [count ($result)]);
        return json_decode($result);
    }

    public function actionUpdate($id)
{
    if (!\Yii::$app->user->can('updateOwnProfile', ['profileId' => \Yii::$app->user->id])) {
        throw new ForbiddenHttpException('Access denied');
    }
} 
}




