<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Day;


class DayController extends Controller
{
    /* public function init()
     {

     }*/

    /**
     * {@inheritdoc}
     */
    /* public function behaviors()
     {
         return [
             'access' => [
                 'class' => AccessControl::className(),
                 'only' => ['logout'],
                 'rules' => [
                     [
                         'actions' => ['index'],
                         'allow' => true,
                         'roles' => ['@'],
                     ],
                 ],
             ],
             'verbs' => [
                 'class' => VerbFilter::className(),
                 'actions' => [
                     'logout' => ['index'],
                 ],
             ],
         ];
     }*/


    //Контроллер возвращает праздники заданного дня в JSON формате
    public function actionIndex($day)
    {
        $model = New Day();

        if ($model->load(['day' => $day], '')) {

            $result = $model->getDay();

            return !empty($result) ? $result : [[
                "guid" => 0,
                "name" => "Праздники отсутствуют в этот день",
                "text" => "Праздники отсутствуют в этот день",
                "url" => "voidcelebrationday.jpg",
                "active" => "Y",
                "sort" => "100",
                "describe_header" => "Праздники отсутствуют в этот день",
                "describe_main" => "Праздники отсутствуют в этот день",
                "img_icon" => "voidcelebrationday.jpg",
                "img_preview" => "voidcelebrationday.jpg"
                ]
            ];
        }

    }


}