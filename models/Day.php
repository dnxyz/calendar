<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Day extends Model
{
    public $day;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['day'], 'required'],
            ['day', 'validateDay'],
        ];
    }

    public function validateDay()
    {
        if (strlen($this->day) != 8) {
            throw new \Exception ("Неверная длинна");
        }
        if (!checkdate(
            substr($this->day, 4, 2),
            substr($this->day, 6, 2),
            substr($this->day, 0, 4)
        )) {
            throw new \Exception ("Неверный формат даты");
        };
    }

    public function getDay()
    {
        $this->validate();

        return Yii::$app->db->createCommand('SELECT
  `guid`,
  `name`,
  `text`,
  `url`,  
  `active`,
  `sort`,
  `describe_header`,
  `describe_main`,
  `img_icon`,
  `img_preview`
 FROM celebration
 WHERE month = '.substr($this->day, 4, 2).' 
 AND day = '.substr($this->day, 6, 2)
        )
            ->queryAll();
    }
}

