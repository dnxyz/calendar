module.exports = {
    //option
    //Basic path
    publicPath: './',
    //Output directory at build time
    outputDir: 'dist',
    //Set directory for static resources
    assetsDir: './',
    //Output path of html
    indexPath: 'index.html',
    //File name hash
    filenameHashing: true,
    //Whether to use it when saving`eslint-loader`Inspection
    lintOnSave: true,
    //Use full build with in browser editor
    runtimeCompiler: true,
    //  Babel loader will skip node modules dependency by default.
    transpileDependencies: [ /* string or regex */ ],
    //  Build build for production environment or not source map？
    productionSourceMap: false,
    //  Set in generated HTML <link rel="stylesheet"> and <script> Labelling crossorigin Attribute.
    crossorigin: "",
    //  In the generated HTML <link rel="stylesheet"> and <script> Enabled on label Subresource Integrity (SRI). 
    integrity: false,
    //  Adjust the internal webback configuration
    configureWebpack: (config) => {
        devtool: "eval-source-map"
    }, //(Object | Function)
    css: {
        sourceMap: true
    },
    chainWebpack: (config) => {
        // Because it is multi page, cancel chunks, and each page only corresponds to a separate JS / CSS
        config.optimization
            .splitChunks({
                cacheGroups: {}
        });

        
    },
    // Configure the webpack dev server behavior.
    devServer: {
        open: process.platform === 'darwin',
        host: '0.0.0.0',
        port: 8080,
        https: false,
        hotOnly: false,
        disableHostCheck: true
    },
    // Enable multiprocess processing babel compilation at build time
    parallel: require('os').cpus().length > 1,

    // https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa
    pwa: {},

    // Third party plug-in configuration
    pluginOptions: {}
}