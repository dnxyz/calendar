/* eslint-disable */
import Vue from 'vue'
import App from './App'
import Login from './components/Login'
import Root from './components/Root'  

import VueRouter from 'vue-router'

Vue.use(VueRouter)

Vue.config.productionTip = false

const indexPage =             { template: '<App/>'                }
const loginPage =             { template: '<Login/>'              }
const rootPage =              { template: '<Root/>'               }

const routes = [
  { 
    path: '/', 
    component: indexPage 
  },
  { 
    path: '/login', 
    component: loginPage 
  },
  { 
    path: '/root', 
    component: rootPage
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

let temp4load;

if (location.pathname==='/') {
  temp4load = indexPage.template;
}
if (location.pathname==='/login') {
  temp4load = loginPage.template;
}
if (location.pathname==='/root') {
  temp4load = rootPage.template;
}
/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App, router, Login, Root },
  template: temp4load,
})
