/* eslint-disable */
import axios from 'axios'
import cDateUtil from "./dateUtil.js"

function fetchCelebrationByDayOfMonthJSON(month, day) {
    var cDate = new Date()
    var year = cDate.getFullYear()
    return axios.get('http://calendar.loc/day/' + year + cDateUtil.getMonthWithLeedZero(month) + cDateUtil.getDayWithLeedZero(day))
}

export function fetchCelebrationByDayOfMonth(month, day) {
    return fetchCelebrationByDayOfMonthJSON(month, day)
}