<?php

use yii\db\Migration;

/**
 * Class m210422_031529_add_celebrate_column_text_img
 */
class m210422_031529_add_celebrate_column_text_img extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('celebration', "describe_header", $this->text()->notNull());
        $this->addColumn('celebration', "describe_main", $this->text()->notNull());
        $this->dropColumn('celebration', "date");
        $this->addColumn('celebration', "img_icon", $this->string(255)->notNull());
        $this->addColumn('celebration', "img_preview", $this->string(255)->notNull());      
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210422_031529_add_celebrate_column_text_img cannot be reverted.\n";
        $this->dropColumn('celebration', "describe_header");
        $this->dropColumn('celebration', "describe_main");
        $this->dropColumn('celebration', "img_icon");
        $this->dropColumn('celebration', "img_preview");


        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210422_031529_add_celebrate_column_text_img cannot be reverted.\n";

        return false;
    }
    */
}
