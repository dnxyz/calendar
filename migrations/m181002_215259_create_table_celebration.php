<?php

use yii\db\Migration;

/**
 * Class m181002_215259_create_table_celebration
 */
class m181002_215259_create_table_celebration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('celebration', [
            'id' => $this->primaryKey(),
            'guid' => $this->char(36)->notNull()->unique(),
            'name' => $this->string(255)->notNull(),
            'date' => $this->datetime()->notNull(),
            'text' => $this->text(),
            'url' => $this->string(255),
            'created_at' => $this->datetime()->notNull()->defaultExpression("CURRENT_TIMESTAMP"),
            'updated_at' => $this->datetime()->notNull()
                ->defaultExpression("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
            'active' => $this->char(1)->notNull()->defaultValue('Y'),
            'sort' => $this->integer(11)->notNull()->defaultValue(100),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('celebration');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181002_215259_create_table_celebration cannot be reverted.\n";

        return false;
    }
    */
}
