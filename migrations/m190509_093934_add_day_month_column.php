<?php

use yii\db\Migration;

/**
 * Class m190509_093934_add_day_month_column
 */
class m190509_093934_add_day_month_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('celebration', "month", $this->integer());
        $this->addColumn('celebration', "day", $this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('celebration', 'month');
        $this->renameColumn('celebration', 'day');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190509_093934_add_day_month_column cannot be reverted.\n";

        return false;
    }
    */
}
